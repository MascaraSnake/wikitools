# wikitools

This is a collection of Python scripts that automatically generate data for the [SRB2 Wiki](wiki.srb2.org).

## mobj_tables.py

This generates infotables for [Object types](wiki.srb2.org/wiki/User:MascaraSnake/Object_tables) and [states](wiki.srb2.org/wiki/User:MascaraSnake/State_table). It reads directly from the `states` and `mobjinfo` definitions in `info.c`. Place your SRB2 repository at `../../STJr/SRB2/` (or change the path in the script).

Usage:
* Run `python3 mobj_tables.py` to generate infotables for all Object types and one large infotable for all states. They are written to `mobj_tables_all.txt` and `state_tables_all.txt`, respectively.
* Run `python3 mobj_tables.py mobj` to generate only the Object type infotables.
* Run `python3 mobj_tables.py mobj MT_EXAMPLE EXMPA1` to generate only the infotable for `MT_EXAMPLE`. It is written to `mobj_tables_MT_EXAMPLE.txt`. `EXMPA1` is the sprite displayed in the infobox.
* Run `python3 mobj_tables.py states` to generate only the infotable for all states.
* Run `python3 mobj_tables.py states MT_EXAMPLE S_EXAMPLE_1 S_EXAMPLE_2` to generate an infotable for all states between `S_EXAMPLE_1` and `S_EXAMPLE_2`. The header of the table will read "States &ndash; MT_EXAMPLE". It is written to `state_tables_MT_EXAMPLE.txt`.

Known issues:
* Currently, the script doesn't handle `#ifdef`'s properly, so you'll have to remove them from the`states` and `mobjinfo` definitions manually before running the script.
* If you generate all Object type infotables at once, the sprite will default to `POSSA1`. This is because sprites are defined in the states, not the Object type definition itself. Maybe in the future I'll make it default to the sprite used by the Object type's `SpawnState`.

## thing_counter.py

This generates a table that counts all the occurrences of each Thing type in a map. These tables appear in the "Technical data" sections of the level pages, e.g., [Greenflower Zone Act 1](wiki.srb2.org/wiki/Greenflower_Zone_Act_1#Technical_data). The list of Thing types is read from `thing_names.txt`, which is a slightly cleaned-up version of the [Thing types](wiki.srb2.org/wiki/Thing_types) page. Before running the script, check that the Thing type list is up to date.

You can run this script on WAD or PK3 files. Run `python3 thing_counter.py example.pk3` to generate Thing tables for all maps inside `example.pk3` (same for WAD files). For each map `MAPxx` inside the file, the script will generate a text file `example.pk3_MAPxx.txt` with the Thing table.