import glob, os, sys, zipfile

thing_names = ['' for x in range(4096)]
thing_categories = []
ring_values = { 300: 1, 308: 1, 309: 1, 400: 10, 414: 10, 415: 10, 600: 5, 601: 5, 602: 5, 603: 10, 604: 8, 605: 16, 608: 4, 609: 8, 1800: 1 }
rings_category = -1

def parse_thing_names():
    with open('thing_names.txt', 'r') as name_file:
        for line in name_file:
            line = line.strip()
            if not line:
                continue
            if line.startswith('=='):
                thing_categories.append([line.strip('='), []])
            else:
                type, name = line.strip().split(': ', 1)
                type = int(type.strip('* Thing type '))
                if type == 300:
                    global rings_category
                    rings_category = thing_categories[-1][0]
                thing_names[type] = name
                thing_categories[-1][1].append(type)

def parse_things(output_filename, data, lump_start, lump_size):
    thing_count = [0 for x in range(4096)]
    
    for i in range(lump_size//10):
        index = lump_start + i*10
        type = int.from_bytes(data[index+6:index+8], "little", signed=False)
        thing_count[type % 4096] += 1
        
    total_rings = 0
    for type, value in ring_values.items():
        total_rings += value * thing_count[type]
    
    with open(output_filename + '.txt', 'w') as output_file:
        output_file.write('{| class="wikitable collapsible collapsed"\n')
        output_file.write('|-\n')
        output_file.write('! colspan="2" | [[Thing types|Things]]\n')
                        
        for [name, types] in thing_categories:
            header_written = False
            for type in types:
                if thing_count[type] == 0:
                    continue
                if not header_written:
                    output_file.write('|-\n')
                    output_file.write('! [[Thing types#' + name + '|' + name + ']]\n')
                    output_file.write('! Occurrences\n')
                    header_written = True
                output_file.write('|-\n')
                output_file.write('| ' + thing_names[type] + '\n')
                output_file.write('| ' + str(thing_count[type]) + '\n')
            if header_written and name == rings_category:
                output_file.write('|-\n')
                output_file.write('| \'\'\'Total rings\'\'\'\n')
                output_file.write('| ' + str(total_rings) + '\n')
        output_file.write('|}\n')
        
def read_wad(filename, input_file, ignore_mapname):
    header_size = 12
    type = input_file.read(4)
    assert type == b'PWAD', 'Input file is not a WAD file with PWAD header!'
    num_lumps = int.from_bytes(input_file.read(4), "little")
    directory_offset = int.from_bytes(input_file.read(4), "little")
    data = input_file.read(directory_offset - header_size)
    mapname = ''
    
    for i in range(num_lumps):
        lump_start = int.from_bytes(input_file.read(4), "little") - header_size
        lump_size = int.from_bytes(input_file.read(4), "little")
        lump_name = input_file.read(8).decode('UTF-8').rstrip('\0')
        if lump_size == 0:
            mapname = lump_name
        if lump_name == 'THINGS':
            parse_things(filename if ignore_mapname else filename + '_' + mapname, data, lump_start, lump_size)

    
def open_wad(filename):
    with open(filename, 'rb') as input_file:
        read_wad(filename, input_file, False)
                
def open_pk3(filename):
    with zipfile.ZipFile(filename, 'r') as input_file:
        for lumpinfo in input_file.infolist():
            if not lumpinfo.filename.endswith('.wad'):
                continue
            mapname = lumpinfo.filename.split('/')[-1].rstrip('.wad')
            print('Parsing ' + mapname + '...')
            lumpinfo.filename = 'tmp.wad'
            input_file.extract(lumpinfo)
            with open('tmp.wad', 'rb') as wad_file:
                read_wad(filename + '_' + mapname, wad_file, True)
            os.remove('tmp.wad')

parse_thing_names()

if len(sys.argv) < 2:
    sys.exit('Please specify a file!')
    
if sys.argv[1].endswith('.wad'):
    open_wad(sys.argv[1])
elif sys.argv[1].endswith('.pk3'):
    open_pk3(sys.argv[1])