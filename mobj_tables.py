import sys

mobj_attribute_names = [
    'thingid',
    'spawnstate',
    'spawnhealth',
    'seestate',
    'seesound',
    'reactiontime',
    'attacksound',
    'painstate',
    'painchance',
    'painsound',
    'meleestate',
    'missilestate',
    'deathstate',
    'xdeathstate',
    'deathsound',
    'speed',
    'radius',
    'height',
    'displayoffset',
    'mass',
    'damage',
    'activesound',
    'flags',
    'raisestate'
]

input_file = open('../../STJr/SRB2/src/info.c', 'r')

def read_mobj_info(mobj_file, name, sprite):
    attributes = []
    while True:
        line = input_file.readline().split(',',1)[0].split('//',1)[0].strip()
        if line == '}':
            break
        attributes.append(line)
    assert len(attributes) == len(mobj_attribute_names), 'Mobj definition must have ' + len(mobj_attribute_names) + ' entries!'
    mobj_file.write('{{Object info\n')
    mobj_file.write('| ' + name + '|' + sprite + '| objectid = ' + name + '\n')
    mobj_file.write('| thingid = ' + ('\'\'none\'\'' if attributes[0] == '-1' else attributes[0])  + '\n')
    #mobj_file.write('| spriteid = ' + sprite[0:4] + '\n')
    for i in range(1, len(mobj_attribute_names)):
        mobj_file.write('| ' + mobj_attribute_names[i] + ' = ' + attributes[i].replace('|', '<nowiki>|</nowiki>') + '\n')
    mobj_file.write('}}\n')

def read_mobj_tables(mobjtype, sprite):
    with open('mobj_tables_' + mobjtype + '.txt', 'w') as mobj_file:
        line = input_file.readline().strip()
        assert line == '{', 'Expected {'
        while True:
            line = input_file.readline().strip()
            if line == '};':
                break
            if not line:
                continue
            if line[0] == '{':
                name = line.split('//',1)[1].strip()
                if mobjtype in [name, 'all']:
                    read_mobj_info(mobj_file, name, sprite)

def write_state_table_header(states_file, title):
    states_file.write('{| class="wikitable collapsible collapsed" style="width: 100%;"\n')
    states_file.write('! colspan="9"|States' + ('' if not title else ' &ndash; ' + title) + '\n')
    states_file.write('|-\n')
    states_file.write('!State\n')
    states_file.write('!Sprite prefix\n')
    states_file.write('!Frame\n')
    states_file.write('!Frame flags\n')
    states_file.write('!Duration (tics)\n')
    states_file.write('![[Actions|Action]]\n')
    states_file.write('!Var1\n')
    states_file.write('!Var2\n')
    states_file.write('!Next state\n')

def extract_frame_flags(attributes):
    #A few states (foolishly!) use addition instead of bitwise OR to apply flags
    #Treat addition as bitwise OR anyway and hope nothing breaks
    frame_components = attributes[1].replace(' + ', '<nowiki>|</nowiki>').split('<nowiki>|</nowiki>')
    frame_value = 'A'
    frame_flags_list = []
    for x in frame_components:
        try:
            frame_value = chr(ord('A') + int(x))
        except ValueError:
            if x.startswith('SPR2_'):
                frame_value = x
            else:
                frame_flags_list.append(x)
    frame_flags = "<nowiki>|</nowiki>".join(frame_flags_list)
    attributes[1] = frame_value
    attributes.insert(2, frame_flags)
    return attributes

def read_state_tables(title, start, end):
    line = input_file.readline().strip()
    assert line == '{', 'Expected {'
    suffix = 'all' if title == '' else title
    with open('state_tables_' + suffix + '.txt', 'w') as states_file:
        write_state_table_header(states_file, title)
        started = False
        while True:
            line = input_file.readline().strip()
            if not line:
                continue
            if line == '};':
                break
            line, name = [x.strip() for x in line.split('//',1)]
            if not line:
                continue
            name = name.split(',',1)[0].split('//',1)[0].strip()
            if name == start:
                started = True
            if not started:
                continue
            states_file.write('|-\n')
            states_file.write('|' + name + '\n')
            attributes = line[1:-2].split(',')
            attributes = [x.strip().replace('|', '<nowiki>|</nowiki>').replace('<<', '<nowiki><<</nowiki>').replace('-', '<nowiki>-</nowiki>') for x in attributes]
            attributes = extract_frame_flags(attributes)
            attributes[4] = '\'\'none\'\'' if attributes[4] == '{NULL}' else '[[' + attributes[4][1:-1] + ']]'
            for x in attributes:
                states_file.write('|' + x + '\n')
            if name == end:
                break
        states_file.write('|}\n')

mode = sys.argv[1] if len(sys.argv) > 1 else 'all'
if mode not in ['mobj', 'states', 'all']:
    sys.exit('Unknown mode! (mobj/states/all)')

while True:
    line = input_file.readline()
    if not line:
        break
    line = line.strip()
    if mode in ['mobj', 'all'] and line == 'mobjinfo_t mobjinfo[NUMMOBJTYPES] =':
        mobjtype = sys.argv[2] if mode == 'mobj' and len(sys.argv) > 2 else 'all'
        sprite = sys.argv[3] if mode == 'mobj' and len(sys.argv) > 3 else 'POSSA1'
        read_mobj_tables(mobjtype, sprite)
    elif mode in ['states', 'all'] and line == 'state_t states[NUMSTATES] =':
        title = sys.argv[2] if mode == 'states' and len(sys.argv) > 4 else ''
        start = sys.argv[3] if mode == 'states' and len(sys.argv) > 4 else 'S_NULL'
        end = sys.argv[4] if mode == 'states' and len(sys.argv) > 4 else 'DUMMY'
        read_state_tables(title, start, end)

input_file.close()